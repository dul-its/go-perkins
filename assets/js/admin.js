$(document).ready(function() {
	$('#uploadMockupForm').on('submit', function(evt) {
		evt.preventDefault();
		var formData = new FormData( $(this)[0] );
		var action = $(this).attr('action');
		$.ajax({
			url						: $(this).attr('action'),
			type					: 'POST',
			data					: formData,
			async					: false,
			cache					: false,
			enctype				: 'multipart/form-data',
			processData		: false,
			success				: function (response, status, o) {
				var i = "hi";
				debugger;
			}
		})
	});

	$('#btnSelectRoom').on('click', function(evt) {
		var selected = $(':selected', '#namingOpportunities').val();
		if (selected != "0") {
			// proceed.
			var url = "/namingopportunities/admin/name-space/" + selected;
			window.location.href = url;
		}
	});

	// button click handler for name-space
	$('button[type="submit"]').on('click', function(evt) {
		if (confirm("Are you sure...?") == true) {
			$(this).parents('form').submit();
		}
		return evt.preventDefault();
	});

	CKEDITOR.replace( 'introText' );
});