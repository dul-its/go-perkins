package main

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"net/http"
	"regexp"
	"text/template"
	"strconv"
)

type EventsPageContext struct {
	Title string
	Body  []byte
}

type EventsContext struct {
	Rows []*EventRow
}

type OccurrenceRow struct {
	Dbid             string
	WhenEnd          string
	WhenStart        string
	RegistrationType string
	OccurrenceStatus string
	WaitList         string
}

type EventRow struct {
	EventDescription string
	EventName        string
	Dbid             string
	PublicationStatus	string
	Occurrences      []*OccurrenceRow
}

func (c *EventsPageContext) Write(buf []byte) (int, error) {
	c.Body = append(c.Body, buf...)
	return 0, nil
}

var eventsRegexp = myRegexp{regexp.MustCompile("/events(/((?P<owner>\\w+))/((?P<section>occurrence|registrants)/)?(?P<dbid>\\d+))")}

func ShowEventHandler(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	vars := mux.Vars(r)
	id := vars["id"]

	// create an interface{} context to return back to the caller
	pageContext := &EventsPageContext{Title: "Events", Body: []byte("")}

	// db, err := sql.Open("mysql", "event_user:ev3nt5@/event")
	db, err := dbSession();
	if err != nil {
		return nil, fmt.Errorf("ShowEventHandler: error opening database connection: %v", err)
	}

	query := "SELECT event.dbid, " +
			 "eventDescription, " + 
			 "eventName " +
			 "FROM event " +
			 "LEFT JOIN owner ON event.ownerId = owner.dbid " +
			 "WHERE event.dbid = ? AND owner.code = ?"
			  
	result := db.QueryRow(query, id, vars["owner"])

	context := &EventRow{}
	if err = result.Scan(&context.Dbid, &context.EventDescription, &context.EventName); err != nil {
		return nil, fmt.Errorf("ShowEventHandler: error with scanning row (%+v)", err)
	}
	pageContext.Title = context.EventName

	query = "SELECT dbid, " +
			"DATE_FORMAT(whenEnd, '%b %e, %Y - %h:%i %p') AS whenEnd, " +
			"DATE_FORMAT(whenStart, '%b %e, %Y - %h:%i %p') AS whenStart, " +
			"registrationType, " +
			"occurrenceStatus, " +
			"waitList " +
			"FROM occurrence WHERE eventId = ?"
		
	rows, err := db.Query(query, id)
	defer rows.Close()
	for rows.Next() {
		o := &OccurrenceRow{}
		if err = rows.Scan(&o.Dbid, &o.WhenEnd, &o.WhenStart, &o.RegistrationType, &o.OccurrenceStatus, &o.WaitList); err != nil {
			return nil, fmt.Errorf("ShowEventHandler: unable to scan occurrence row - %v", err)
		}
		context.Occurrences = append(context.Occurrences, o)
	}

	// this data interface will contain the rows from our SQL query
	bodyTemplate := template.Must(template.ParseFiles("templates/libevents/showevent.html", "templates/libevents/occurrences.html"))
	err = bodyTemplate.Execute(pageContext, context)

	if err != nil {
		return nil, fmt.Errorf("ShowEventHandler: unable to execute template for EventRow - %v", err)
	}

	return *pageContext, nil
}

// Handle the request for paths beginning with "/events"
// It returns an interface{} struct and any errors encountered
func EventsHomeHandler(w http.ResponseWriter, r *http.Request) (interface{}, error) {
	// create an interface{} context to return back to the caller
	pageContext := &EventsPageContext{Title: "Events", Body: []byte("")}
	
	// what page is the user looking to access?
	var queryPage int
	queryPage, _ = strconv.Atoi(r.FormValue("page"))
	var itemsPerPage int = 15
	var recordStart int = queryPage * itemsPerPage
	var sqlQuery string = fmt.Sprintf("SELECT dbid, eventDescription, eventName, publicationStatus FROM event ORDER by dbid DESC LIMIT %d, %d", recordStart, itemsPerPage)

	db, err := sql.Open("mysql", "event_user:ev3nt5@/event")
	if err != nil {
		return nil, fmt.Errorf("LibEventsHander: error opening database connection: %v", err)
	}
	resultSet, err := db.Query(sqlQuery)
	if err != nil {
		return nil, fmt.Errorf("LibEventsHandler: error with query: %v", err)
	}

	rows := make([]*EventRow, 0)

	defer resultSet.Close()
	for resultSet.Next() {
		var dbid string
		var eventDescription string
		var eventName string
		var publicationStatus string
		if err := resultSet.Scan(&dbid, &eventDescription, &eventName, &publicationStatus); err != nil {
			return nil, fmt.Errorf("LibEventsHandler: error with row - %v", err)
		}
		eventRow := &EventRow{Dbid: dbid, EventName: eventName, EventDescription: eventDescription, PublicationStatus: publicationStatus}
		rows = append(rows, eventRow)
	}

	// this data interface will contain the rows from our SQL query
	context := &EventsContext{Rows: rows}
	bodyTemplate := template.Must(template.ParseFiles("templates/libevents/main.html"))
	err = bodyTemplate.Execute(pageContext, context)

	if err != nil {
		return nil, fmt.Errorf("LibEventsHandler: unable to execute template for EventRow - %v", err)
	}
	return *pageContext, nil
}
