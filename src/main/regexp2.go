package main

// code from here:
// http://blog.kamilkisiel.net/blog/2012/07/05/using-the-go-regexp-package/

import (
	"regexp"
)

// embed regex.Regexp in a new type so we can extend it
type myRegexp struct {
	*regexp.Regexp
}

func (r *myRegexp) FindStringSubmatchMap(s string) map[string]string {
	captures := make(map[string]string)
	
	match := r.FindStringSubmatch(s)
	if match == nil {
		return captures
	}
	
	for i, name := range r.SubexpNames() {
		if i == 0 || name == "" {
			continue
		}
		
		captures[name] = match[i]
	}
	return captures
}