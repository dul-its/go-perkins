// Package main is where all the fun starts for go_perkins
// includes main()
package main

import (
	_ "database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
	"io/ioutil"
	"net/http"
	"os"
	"regexp"
	"text/template"
)

type Page struct {
	Title string
	Body  []byte
}

type DULPage struct {
	Layout   string
	Template *template.Template
	Content  []byte
}

func (p *Page) save() error {
	filename := p.Title + ".txt"
	return ioutil.WriteFile(filename, p.Body, 0600)
}

func loadPage(title string) (*Page, error) {
	filename := title + ".txt"
	body, err := ioutil.ReadFile(filename)
	if err != nil {
		return nil, err
	}
	return &Page{Title: title, Body: body}, nil
}

func ViewHandler(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	title := vars["title"]
	p, _ := loadPage(title)

	t := template.Must(template.ParseFiles("assets/templates/layout.html"))
	t.Execute(w, p)

	//fmt.Fprintf(w, "<h1>%s</h1><div>%s</div>", p.Title, p.Body)
}

/**
 * /assets/(css|js|img)/<assetname>
 */
func assetsHandler(w http.ResponseWriter, r *http.Request) {
	pwd, _ := os.Getwd()
	fmt.Printf("%s\n", pwd)

	var myExp = myRegexp{regexp.MustCompile("/assets/.*\\.(?P<extension>css|jpg|gif|png|jpeg|js)$")}
	matches := myExp.FindStringSubmatchMap(r.URL.Path)

	assetPath := r.URL.Path[len("/"):]
	body, err := ioutil.ReadFile(assetPath)
	if err != nil {
		panic(fmt.Sprintf("main: Unable to open asset, %s", assetPath))
	}

	var contentType = ""
	switch matches["expression"] {
	case "css":
		contentType = "text/css;charset=UTF-8"
	case "jpg", "jpeg":
		contentType = "image/jpeg"
	case "png":
		contentType = "image/png"
	case "gif":
		contentType = "image/gif"
	case "js":
		contentType = "text/javascript;charset=UTF-8"
	}
	w.Header().Set("Content-type", contentType)

	fmt.Fprintf(w, "%s", body)
	fmt.Printf("Assets handler launched\n")
}

// load and parse the MASTER DUL template here
// (so as to be efficient and not call this for every handler)
// and have the template's "Execute" write into 'w'
// prior to deferring to the app-specific handler
var masterTemplate = template.Must(template.ParseFiles("templates/master/dul_master.html"))

// Creates a master template then executes the function handler (f)
// to receive a data context (interface{}) which is then applied to the
// master template
// returns a 'handler' function that processes the request
func errorHandler(f func(http.ResponseWriter, *http.Request) (interface{}, error)) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		// we need some sort of minimal context here
		// so, we'll ask the function represented by 'f' to return an interface{}
		context, err := f(w, r)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
		// finally, we'll execute the 'masterTemplate' with the context
		// returned by 'f', which should have a "Body" accessor.
		err = masterTemplate.Execute(w, context)
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
		}
	}
}

func main() {
	InitLogging(ioutil.Discard, os.Stdout, os.Stdout, os.Stderr)
	
	r := mux.NewRouter()
	r.PathPrefix("/assets/").
		HandlerFunc(assetsHandler)

	r.HandleFunc("/events/{owner}/occurrence/{id:[0-9]+}", errorHandler(EventsHomeHandler))
	r.HandleFunc("/events/{owner}/{id:[0-9]+}", errorHandler(ShowEventHandler))
	r.HandleFunc("/events", errorHandler(EventsHomeHandler))
	r.HandleFunc("/view/{title}", ViewHandler)

	http.Handle("/", r)
	Info.Println("Listening and serving on port 8080")
	http.ListenAndServe(":8080", nil)
}
