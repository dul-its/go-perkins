package main

import (
	"database/sql"
	_ "github.com/go-sql-driver/mysql"
)

func dbSession() (*sql.DB, error) {
	return sql.Open("mysql", "event_user:ev3nt5@/event");
}