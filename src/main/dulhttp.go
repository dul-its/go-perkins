package main

type WebUser struct {
	username	string
	
	// The NetID of a user when he/she is logged in (via Shibbolet, LDAP)
	netID		string
	
	// First name
	firstName	string
	
	// Last name
	lastName	string
}
